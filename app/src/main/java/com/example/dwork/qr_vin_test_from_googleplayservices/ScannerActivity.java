package com.example.dwork.qr_vin_test_from_googleplayservices;

import android.Manifest;
import android.app.ActionBar;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import com.example.dwork.qr_vin_test_from_googleplayservices.detector.MyCameraSource;
import com.example.dwork.qr_vin_test_from_googleplayservices.detector.Globals;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

public class ScannerActivity extends AppCompatActivity {


    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private BarcodeDetector barcodeDetector;
    private MyCameraSource cameraSource;
    private SurfaceView surfaceView;
    private SparseArray<Barcode> barCodes;
    private int screenHeight;
    private int screenWidth;
    private int scannerType;
    private int orientation;
    private boolean isFlashOn = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

//            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
//            }
        }else{
            startScan();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startScan();
                } else {
                    sendAnswer(RESULT_CANCELED, null);
                }
                return;
            }
        }
    }

    private void startScan() {
        getDisplay();
        hideBar();
        getScannerType();
        setOrientation(scannerType);
        initViews();
        initBarCodeDetector(scannerType);
        setReseivProcessor();
        initCameraSource();
        initPreview();
    }

    private void getDisplay() {
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;
        orientation = getResources().getConfiguration().orientation;
    }

    private void hideBar() {
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);

            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.hide();
            }
        }
    }

    private void getScannerType() {
        Intent intent = getIntent();
        scannerType = intent.getIntExtra(Globals.SCANNER, Barcode.QR_CODE);
    }

    private void setOrientation(int scannerType) {
        if (scannerType == Globals.REQUEST_CODE_VIN_SCANNER
                || scannerType == Globals.REQUEST_CODE_PDF417) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void initViews() {
        setContentView(R.layout.scanner_activity);

        surfaceView = (SurfaceView) findViewById(R.id.camera_view);

        final ImageView btnFlashSwitch = (ImageView) this.findViewById(R.id.btnFlashSwitch);
        btnFlashSwitch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Camera.Parameters cameraParams = cameraSource.camParams;

                if (isFlashOn) {
                    btnFlashSwitch.setImageResource(R.drawable.ic_flash_on_black_24dp);
                    cameraParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    isFlashOn = false;
                } else {
                    btnFlashSwitch.setImageResource(R.drawable.ic_flash_off_black_24dp);
                    cameraParams.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    isFlashOn = true;
                }
                cameraSource.mCamera.setParameters(cameraParams);
            }
        });
    }

    private void initBarCodeDetector(int scannerType) {
        barcodeDetector =
                new BarcodeDetector.Builder(this)
                        .setBarcodeFormats(scannerType)
                        .build();
    }

    private void initCameraSource() {

        MyCameraSource.Builder cameraSourceBuilder = new MyCameraSource.Builder(this, barcodeDetector)
                .setRequestedFps(30.0f)
                .setRequestedPreviewSize(screenHeight, screenWidth)
                .setFacing(MyCameraSource.CAMERA_FACING_BACK);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            cameraSourceBuilder = cameraSourceBuilder.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

        cameraSource = cameraSourceBuilder.build();
    }

    private void initPreview() {

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    cameraSource.start(surfaceView.getHolder());
                } catch (IOException ie) {
                    Log.e("tag", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });
    }


    boolean isSent = false;
    private void setReseivProcessor(){

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {

                barCodes = detections.getDetectedItems();

                if (barCodes.size() != 0) {
                    if (!isSent) {
                        isSent = true;
                        sendAnswer(RESULT_OK, barCodes.valueAt(0).displayValue);
                    }
                }
            }
        });
    }

    private void sendAnswer(int status, String answer) {

        Intent intent = new Intent();
        if (answer != null) {
            intent.putExtra(Globals.SCANNER, answer);
        }
        setResult(status, intent);
        finish();
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (cameraSource != null) {
            cameraSource.stop();
        }
    }
}
