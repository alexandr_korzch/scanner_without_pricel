package com.example.dwork.qr_vin_test_from_googleplayservices.detector;


import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import com.google.android.gms.common.images.Size;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class MyCameraSource {

    public static final int CAMERA_FACING_BACK = 0;
    private Context mContext;
    private final Object zzbaE;
    public Camera mCamera;
    public Camera.Parameters camParams;
    private int zzbaG;
    private int zzAF;
    private Size zzbaH;
    private float zzbaI;
    private int mRequestedPreviewWidth;
    private int mRequestedPreviewHeight;
    private boolean zzbaN;
    private Thread zzbaO;
    private MyCameraSource.zzb zzbaP;
    private Map<byte[], ByteBuffer> zzbaQ;

    private String mFocusMode = null;
    private String mFlashMode = null;

    public MyCameraSource start(SurfaceHolder surfaceHolder) throws IOException {
        synchronized(this.zzbaE) {
            if(this.mCamera != null) {
                return this;
            } else {
                this.mCamera = this.createMyCamera();
                this.mCamera.setPreviewDisplay(surfaceHolder);
                this.mCamera.startPreview();
                this.zzbaO = new Thread(this.zzbaP);
                this.zzbaP.setActive(true);
                this.zzbaO.start();
                this.zzbaN = false;
                return this;
            }
        }
    }

    public void stop() {
        synchronized(this.zzbaE) {
            this.zzbaP.setActive(false);
            if(this.zzbaO != null) {
                try {
                    this.zzbaO.join();
                } catch (InterruptedException var5) {
                    Log.d("tag", "Frame processing thread interrupted on release.");
                }

                this.zzbaO = null;
            }

            if(this.mCamera != null) {
                this.mCamera.stopPreview();
                this.mCamera.setPreviewCallbackWithBuffer((Camera.PreviewCallback)null);

                try {
                    if(this.zzbaN) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            this.mCamera.setPreviewTexture((SurfaceTexture)null);
                        }
                    } else {
                        this.mCamera.setPreviewDisplay((SurfaceHolder)null);
                    }
                } catch (Exception var4) {
                    Log.e("tag", "Failed to clear mCamera preview: " + var4);
                }

                this.mCamera.release();
                this.mCamera = null;
            }

        }
    }

    private MyCameraSource() {
        this.zzbaE = new Object();
        this.zzbaG = 0;
        this.zzbaI = 30.0F;
        this.mRequestedPreviewWidth = 1024;
        this.mRequestedPreviewHeight = 768;
        this.zzbaQ = new HashMap();
    }

    private Camera createMyCamera() {
        int var1 = zzjD(this.zzbaG);
        if(var1 == -1) {
            throw new RuntimeException("Could not find requested mCamera.");
        } else {
            Camera var2 = Camera.open(var1);
            MyCameraSource.zze var3 = zza(var2, this.mRequestedPreviewWidth, this.mRequestedPreviewHeight);
            if(var3 == null) {
                throw new RuntimeException("Could not find suitable preview size.");
            } else {
                Size var4 = var3.zzEw();
                this.zzbaH = var3.zzEv();
                int[] var5 = this.zza(var2, this.zzbaI);
                if(var5 == null) {
                    throw new RuntimeException("Could not find suitable preview frames per second range.");
                } else {
                    camParams = var2.getParameters();

                    camParams.setPictureSize(var4.getWidth(), var4.getHeight());
                    camParams.setPreviewSize(this.zzbaH.getWidth(), this.zzbaH.getHeight());
                    camParams.setPreviewFpsRange(var5[0], var5[1]);
                    camParams.setPreviewFormat(17);

                    if (mFocusMode != null) {
                        if (camParams.getSupportedFocusModes().contains(
                                mFocusMode)) {
                            camParams.setFocusMode(mFocusMode);
                        } else {
                            Log.i("tag", "Camera focus mode: " + mFocusMode + " is not supported on this device.");
                        }
                    }

                    mFocusMode = camParams.getFocusMode();

                    if (mFlashMode != null) {
                        if (camParams.getSupportedFlashModes().contains(
                                mFlashMode)) {
                            camParams.setFlashMode(mFlashMode);
                        } else {
                            Log.i("tag", "Camera flash mode: " + mFlashMode + " is not supported on this device.");
                        }
                    }

                    mFlashMode = camParams.getFlashMode();

                    this.zza(var2, camParams, var1);
                    var2.setParameters(camParams);
                    var2.setPreviewCallbackWithBuffer(new MyCameraSource.zza());
                    var2.addCallbackBuffer(this.zza(this.zzbaH));
                    var2.addCallbackBuffer(this.zza(this.zzbaH));
                    var2.addCallbackBuffer(this.zza(this.zzbaH));
                    var2.addCallbackBuffer(this.zza(this.zzbaH));
                    return var2;
                }
            }
        }
    }

    private static int zzjD(int var0) {
        Camera.CameraInfo var1 = new Camera.CameraInfo();

        for(int var2 = 0; var2 < Camera.getNumberOfCameras(); ++var2) {
            Camera.getCameraInfo(var2, var1);
            if(var1.facing == var0) {
                return var2;
            }
        }

        return -1;
    }

    private static MyCameraSource.zze zza(Camera var0, int var1, int var2) {
        List var3 = zza(var0);
        MyCameraSource.zze var4 = null;
        int var5 = 2147483647;
        Iterator var6 = var3.iterator();

        while(var6.hasNext()) {
            MyCameraSource.zze var7 = (MyCameraSource.zze)var6.next();
            Size var8 = var7.zzEv();
            int var9 = Math.abs(var8.getWidth() - var1) + Math.abs(var8.getHeight() - var2);
            if(var9 < var5) {
                var4 = var7;
                var5 = var9;
            }
        }

        return var4;
    }

    private static List<MyCameraSource.zze> zza(Camera var0) {
        Camera.Parameters var1 = var0.getParameters();
        List var2 = var1.getSupportedPreviewSizes();
        List var3 = var1.getSupportedPictureSizes();
        ArrayList var4 = new ArrayList();
        Iterator var5 = var2.iterator();

        while(true) {
            android.hardware.Camera.Size var6;
            while(var5.hasNext()) {
                var6 = (android.hardware.Camera.Size)var5.next();
                float var7 = (float)var6.width / (float)var6.height;

                for(int var8 = var3.size() - 1; var8 >= 0; --var8) {
                    android.hardware.Camera.Size var9 = (android.hardware.Camera.Size)var3.get(var8);
                    float var10 = (float)var9.width / (float)var9.height;
                    if(Math.abs(var7 - var10) < 0.01F) {
                        var4.add(new MyCameraSource.zze(var6, var9));
                        break;
                    }
                }
            }

            if(var4.size() == 0) {
                Log.w("MyCameraSource", "No preview sizes have a corresponding same-aspect-ratio picture size");
                var5 = var2.iterator();

                while(var5.hasNext()) {
                    var6 = (android.hardware.Camera.Size)var5.next();
                    var4.add(new MyCameraSource.zze(var6, (android.hardware.Camera.Size)null));
                }
            }

            return var4;
        }
    }

    private int[] zza(Camera var1, float var2) {
        int var3 = (int)(var2 * 1000.0F);
        int[] var4 = null;
        int var5 = 2147483647;
        List var6 = var1.getParameters().getSupportedPreviewFpsRange();
        Iterator var7 = var6.iterator();

        while(var7.hasNext()) {
            int[] var8 = (int[])var7.next();
            int var9 = var3 - var8[0];
            int var10 = var3 - var8[1];
            int var11 = Math.abs(var9) + Math.abs(var10);
            if(var11 < var5) {
                var4 = var8;
                var5 = var11;
            }
        }

        return var4;
    }

    private void zza(Camera var1, Camera.Parameters var2, int var3) {
        WindowManager var4 = (WindowManager)this.mContext.getSystemService(Context.WINDOW_SERVICE);
        short var5 = 0;
        int var6 = var4.getDefaultDisplay().getRotation();
        switch(var6) {
            case 0:
                var5 = 0;
                break;
            case 1:
                var5 = 90;
                break;
            case 2:
                var5 = 180;
                break;
            case 3:
                var5 = 270;
                break;
            default:
                Log.e("tag", "Bad rotation value: " + var6);
        }

        Camera.CameraInfo var7 = new Camera.CameraInfo();
        Camera.getCameraInfo(var3, var7);
        int var8;
        int var9;
        if(var7.facing == 1) {
            var8 = (var7.orientation + var5) % 360;
            var9 = (360 - var8) % 360;
        } else {
            var8 = (var7.orientation - var5 + 360) % 360;
            var9 = var8;
        }

        this.zzAF = var8 / 90;
        var1.setDisplayOrientation(var9);
        var2.setRotation(var8);
    }

    private byte[] zza(Size var1) {
        int var2 = ImageFormat.getBitsPerPixel(17);
        long var3 = (long)(var1.getHeight() * var1.getWidth() * var2);
        int var5 = (int)Math.ceil((double)var3 / 8.0D) + 1;
        byte[] var6 = new byte[var5];
        ByteBuffer var7 = ByteBuffer.wrap(var6);
        if(var7.hasArray() && var7.array() == var6) {
            this.zzbaQ.put(var6, var7);
            return var6;
        } else {
            throw new IllegalStateException("Failed to create valid buffer for mCamera source.");
        }
    }

    private class zzb implements Runnable {
        private Detector<?> zzbaR;
        private long zzNY = SystemClock.elapsedRealtime();
        private final Object zzpd = new Object();
        private boolean zzbaU = true;
        private long zzbaV;
        private int zzbaW = 0;
        private ByteBuffer zzbaX;

        zzb(Detector<?> var1) {
            this.zzbaR = var1;
        }

        void setActive(boolean active) {
            synchronized(this.zzpd) {
                this.zzbaU = active;
                this.zzpd.notifyAll();
            }
        }

        void zza(byte[] var1, Camera var2) {
            synchronized(this.zzpd) {
                if(this.zzbaX != null) {
                    var2.addCallbackBuffer(this.zzbaX.array());
                    this.zzbaX = null;
                }

                this.zzbaV = SystemClock.elapsedRealtime() - this.zzNY;
                ++this.zzbaW;
                this.zzbaX = (ByteBuffer)MyCameraSource.this.zzbaQ.get(var1);
                this.zzpd.notifyAll();
            }
        }

        public void run() {
            while(true) {
                Frame var1;
                ByteBuffer var2;
                synchronized(this.zzpd) {
                    if(this.zzbaU && this.zzbaX == null) {
                        try {
                            this.zzpd.wait();
                        } catch (InterruptedException var13) {
                            Log.d("tag", "Frame processing loop terminated.", var13);
                            return;
                        }
                    }

                    if(!this.zzbaU) {
                        return;
                    }

                    var1 = (new com.google.android.gms.vision.Frame.Builder())
                            .setImageData(this.zzbaX, MyCameraSource.this.zzbaH.getWidth(),
                                    MyCameraSource.this.zzbaH.getHeight(), 17)
                            .setId(this.zzbaW).setTimestampMillis(this.zzbaV)
                            .setRotation(MyCameraSource.this.zzAF).build();
                    var2 = this.zzbaX;
                    this.zzbaX = null;
                }

                try {
                    this.zzbaR.receiveFrame(var1);
                } catch (Throwable var11) {
                    Log.e("tag", "Exception thrown from receiver.", var11);
                } finally {
                    MyCameraSource.this.mCamera.addCallbackBuffer(var2.array());
                }
            }
        }
    }

    private class zza implements Camera.PreviewCallback {
        private zza() {
        }

        public void onPreviewFrame(byte[] data, Camera camera) {
            MyCameraSource.this.zzbaP.zza(data, camera);
        }
    }

    private static class zze {
        private Size zzbba;
        private Size zzbbb;

        public zze(android.hardware.Camera.Size var1, android.hardware.Camera.Size var2) {
            this.zzbba = new Size(var1.width, var1.height);
            this.zzbbb = new Size(var2.width, var2.height);
        }

        public Size zzEv() {
            return this.zzbba;
        }

        public Size zzEw() {
            return this.zzbbb;
        }
    }


    public static class Builder {
        private final Detector<?> zzbaR;
        private MyCameraSource mCameraSource = new MyCameraSource();

        public Builder(Context context, Detector<?> detector) {
            if(context == null) {
                throw new IllegalArgumentException("No context supplied.");
            } else if(detector == null) {
                throw new IllegalArgumentException("No detector supplied.");
            } else {
                this.zzbaR = detector;
                this.mCameraSource.mContext = context;
            }
        }

        public MyCameraSource.Builder setRequestedFps(float fps) {
            if(fps <= 0.0F) {
                throw new IllegalArgumentException("Invalid fps: " + fps);
            } else {
                this.mCameraSource.zzbaI = fps;
                return this;
            }
        }

        public MyCameraSource.Builder setRequestedPreviewSize(int width, int height) {
            int var3 = 1000000;
            if(width > 0 && width <= 1000000 && height > 0 && height <= 1000000) {
                this.mCameraSource.mRequestedPreviewWidth = width;
                this.mCameraSource.mRequestedPreviewHeight = height;
                return this;
            } else {
                throw new IllegalArgumentException("Invalid preview size: " + width + "x" + height);
            }
        }

        public MyCameraSource.Builder setFacing(int facing) {
            if(facing != 0 && facing != 1) {
                throw new IllegalArgumentException("Invalid mCamera: " + facing);
            } else {
                this.mCameraSource.zzbaG = facing;
                return this;
            }
        }

        public Builder setFocusMode(String mode) {
            mCameraSource.mFocusMode = mode;
            return this;
        }

        public Builder setFlashMode(String mode) {
            mCameraSource.mFlashMode = mode;
            return this;
        }

        public MyCameraSource build() {
            MyCameraSource var10000 = this.mCameraSource;
            MyCameraSource var10003 = this.mCameraSource;
            this.mCameraSource.getClass();
            var10000.zzbaP = var10003.new zzb(this.zzbaR);
            return this.mCameraSource;
        }
    }
}
