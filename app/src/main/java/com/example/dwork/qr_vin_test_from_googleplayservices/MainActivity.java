package com.example.dwork.qr_vin_test_from_googleplayservices;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.dwork.qr_vin_test_from_googleplayservices.detector.Globals;

public class MainActivity extends AppCompatActivity {


    int scannerType;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        textView = (TextView) findViewById(R.id.textView);
        Button button = (Button) findViewById(R.id.button);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    scannerType = Globals.REQUEST_CODE_QR_SCANNER;
                }else if(position == 1){
                    scannerType = Globals.REQUEST_CODE_VIN_SCANNER;
                }else if(position == 2){
                    scannerType = Globals.REQUEST_CODE_PDF417;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanBarCode();
            }
        });
    }

    private void scanBarCode() {
        Intent intent = new Intent(this, ScannerActivity.class);
        intent.putExtra(Globals.SCANNER, scannerType);
        startActivityForResult(intent, scannerType);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == scannerType) {
                String answer = data.getStringExtra(Globals.SCANNER);
                textView.setText(answer);
            }

        } else {
            Log.d("tag", "resultCode != Activity.RESULT_OK");
        }
    }
}
