package com.example.dwork.qr_vin_test_from_googleplayservices.detector;

import com.google.android.gms.vision.barcode.Barcode;

/**
 * Created by Alexandr on 06.04.2016.
 */
public class Globals {

    public static final String SCANNER = "scanner";
    public static final int REQUEST_CODE_VIN_SCANNER = Barcode.CODE_39;
    public static final int REQUEST_CODE_QR_SCANNER = Barcode.QR_CODE;
    public static final int REQUEST_CODE_PDF417 = Barcode.PDF417;
}
